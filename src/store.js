import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: localStorage
})

export default new Vuex.Store({
  state: {
    articles: [{}],
    mercuryArticles: [{}]
  },
  mutations: {
    setArticles(state,data) {
      state.articles = data;
    },
    setMercuryArticles(state,data) {
       state.mercuryArticles = data;
    },
  },
  actions: {

  },
  plugins: [vuexPersist.plugin]
})
